--Labyrinth - A Factorio PvP Scenario
--Created by zackman0010 of 3RaGaming
--This is the main file, which contains only functions regarding gameplay

require "locale/utils/event"
require "locale/utils/admin"
require "locale/utils/bot"

cell_captured = cell_captured or script.generate_event_name()
cell_processed = cell_processed or script.generate_event_name()
game_ended = game_ended or script.generate_event_name()
game_started = game_started or script.generate_event_name()
phase_change = phase_change or script.generate_event_name()

colors = {
	["green"] = {r=100/255,g=200/255,b=0},
	["blue"] = {r=0,g=100/255,b=200/255}
}

require "generation"
require "gui"
require "win"

local function reset_globals()
	--Internal options set by code
	--THESE ARE NOT MEANT TO BE MANUALLY CHANGED AT ANY TIME
	--MANUALLY CHANGING THESE VALUES IN-GAME WILL BREAK THE SCENARIO
	global.generating = false
	global.started = false
	global.mazes = {}
	global.start_tick = -1
	global.phase = -1
	global.top_left = {x = 0, y = 0}
	global.bottom_right = {x = 0, y = 0}
	global.control_behaviors = {}
	global.cell_control = {}
	global.capturing = {}
	global.owned = {["Green"] = 0, ["Blue"] = 0} --Counts how many cells are owned by each force, used in Domination mode
	global.points = {["Green"] = 0, ["Blue"] = 0} --Tracks how many points each team has earned, used in Elimination mode
	global.bonuses = {}
	global.refill_energy = {} --Bonuses in the Labyrinth that require energy
	global.markets = {} --Used to lock down markets if enemies are present in the cell
	global.offers = {} --Tracks what offers haven't been used
	global.ores = {} --Used to refill ores
	global.uranium_mines = {} --Needed to refill sulfuric acid
	global.assembling_machines = {} --Provide assembling machines with ingredients every 10 seconds
	global.interfaces = {} --Find the electric energy interface entity easier
	global.roboports = {} --Track the location of roboports
	global.radars = {} --Track the location of radars
	global.spawners = 0 -- With biter expansion disabled, there will only ever be a limited number of spawners
end

Event.register(game_ended, function(event) reset_globals() end) --luacheck: ignore event

Event.register(-1, function()
	--Config options set at beginning of each round
	global.total_phases = 5
	global.mazesize = 15
	global.cellsize = 29
	global.delay = 5 --In minutes
	global.capture_time = 10 --In seconds
	global.biters = false
	global.win_condition = "Domination"
	global.domination_limit = 51 --Percent of cells to control in order to win in Domination
	global.elimination_limit = 1000 --Number of points to get in order to win in Elimination
	
	--Debug options, set using the debug menu (Only appears to zackman0010)
	global.open_all_gates = false
	global.resume_gates = false
	global.activate_bonuses = false
	global.deactivate_bonuses = false
	
	reset_globals()
	game.map_settings.enemy_expansion.enabled = false --Prevent biters from expanding naturally
	
	game.create_force("Labyrinth")
	game.create_force("Green")
	game.create_force("Blue")
	
	game.forces.Labyrinth.set_cease_fire("Green", true)
	game.forces.Labyrinth.set_cease_fire("Blue", true)
	game.forces.Labyrinth.research_all_technologies()

	global.round_number = 0
end)

--Helper function: Given a position, find the cell in the Labyrinth
local function find_cell(position)
	local length_tiles = (global.mazesize * global.cellsize) + global.mazesize
	local x = math.ceil((position.x + math.floor(length_tiles / 2)) / (global.cellsize + 1))
	local y = math.ceil((position.y + math.floor(length_tiles / 2)) / (global.cellsize + 1))
	if x > 0 and y > 0 and x <= global.mazesize and y <= global.mazesize then
		return {x=x,y=y}
	else
		return nil
	end
end

--Helper function to count the military assets in an area, used for determining cell capture status
local function get_military_assets(force, top_left, bottom_right)
	local count = 0
	count = count + game.surfaces["Labyrinth"].count_entities_filtered{name="player", force=force, area={top_left, bottom_right}}
	count = count + game.surfaces["Labyrinth"].count_entities_filtered{type="ammo-turret", force=force, area={top_left, bottom_right}}
	count = count + game.surfaces["Labyrinth"].count_entities_filtered{type="electric-turret", force=force, area={top_left, bottom_right}}
	count = count + game.surfaces["Labyrinth"].count_entities_filtered{type="fluid-turret", force=force, area={top_left, bottom_right}}
	return count
end

--Creates a biter base in a cell
local function create_biter_base(cell)
	local top_left = {x = global.top_left.x + ((cell.x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((cell.y - 1) * (global.cellsize + 1))}
	local bottom_right = {x = top_left.x + global.cellsize + 1, y = top_left.y + global.cellsize + 1}
	local bottom_left = {x = top_left.x, y = top_left.y + global.cellsize + 1}
	local top_right = {x = top_left.x + global.cellsize + 1, y = top_left.y}
	local left_center = {x = top_left.x, y = (top_left.y + bottom_left.y) / 2}
	local right_center = {x = top_right.x, y = (top_right.y + bottom_right.y) / 2}
	local top_center = {x = (top_left.x + top_right.x) / 2, y = top_left.y}
	local bottom_center = {x = (bottom_left.x + bottom_right.x) / 2, y = bottom_left.y}
	local center = {x = ((top_left.x + bottom_right.x) / 2) + 3, y = ((top_left.y + bottom_right.y) / 2)}
	game.surfaces.Labyrinth.create_entity{name="spitter-spawner", force="enemy", position = {x = top_left.x + 6, y = top_left.y + 6}}
	game.surfaces.Labyrinth.create_entity{name="spitter-spawner", force="enemy", position = {x = bottom_right.x - 3, y = bottom_right.y - 6}}
	game.surfaces.Labyrinth.create_entity{name="spitter-spawner", force="enemy", position = {x = bottom_left.x + 6, y = bottom_left.y - 6}}
	game.surfaces.Labyrinth.create_entity{name="spitter-spawner", force="enemy", position = {x = top_right.x - 3, y = top_right.y + 6}}
	game.surfaces.Labyrinth.create_entity{name="biter-spawner", force="enemy", position = {x = top_center.x + 1, y = top_center.y + 6}}
	game.surfaces.Labyrinth.create_entity{name="biter-spawner", force="enemy", position = {x = bottom_center.x + 1, y = bottom_center.y - 6}}
	game.surfaces.Labyrinth.create_entity{name="biter-spawner", force="enemy", position = {x = left_center.x + 6, y = left_center.y}}
	game.surfaces.Labyrinth.create_entity{name="biter-spawner", force="enemy", position = {x = right_center.x - 3, y = right_center.y}}

	local biter_size
	if game.forces.enemy.evolution_factor < 0.25 then biter_size = "small"
	elseif game.forces.enemy.evolution_factor < 0.6 then biter_size = "medium"
	elseif game.forces.enemy.evolution_factor < 0.95 then biter_size = "big"
	else biter_size = "behemoth" end
	for _=1,10 do
		local position = game.surfaces.Labyrinth.find_non_colliding_position(biter_size .. "-biter", center, math.floor(global.cellsize / 2), 1)
		if position then game.surfaces.Labyrinth.create_entity{name = biter_size .. "-biter", force = "enemy", position = position} end
	end
	for _=1,15 do
		local position = game.surfaces.Labyrinth.find_non_colliding_position(biter_size .. "-spitter", center, math.floor(global.cellsize / 2), 1)
		if position then game.surfaces.Labyrinth.create_entity{name = biter_size .. "-spitter", force = "enemy", position = position} end
	end

	global.spawners = global.spawners + 8
end

--Causes the biters to attack a player-owned cell
local function prepare_attack(cell)
	local top_left = {x = global.top_left.x + ((cell.x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((cell.y - 1) * (global.cellsize + 1))}
	local bottom_right = {x = top_left.x + global.cellsize + 1, y = top_left.y + global.cellsize + 1}
	local center = {x = ((top_left.x + bottom_right.x) / 2) + 3, y = ((top_left.y + bottom_right.y) / 2)}
	game.surfaces.Labyrinth.set_multi_command{command = {type=defines.command.attack_area, destination=center, radius=global.cellsize / 2}, unit_count=50, force="enemy"}
end

local announcements = {
	{"announcements.announcement1"},
	{"announcements.announcement2"},
	{"announcements.announcement3"}
}

Event.register(defines.events.on_tick, function(event) --luacheck: ignore event
	--Announcements code
	if game.tick % (60 * 60 * 10) == 0 then
		global.current_message = global.current_message or 1
		game.print(announcements[global.current_message])
		global.current_message = (global.current_message == #announcements) and 1 or global.current_message + 1
	end

	if global.started then
		--When time for phase change
		if ((game.tick - global.start_tick) % (60 * 60 * global.delay) == 0 and not global.open_all_gates) or global.resume_gates then
			global.open_all_gates = false
			global.resume_gates = false
			if global.total_phases == 0 and game.tick ~= global.start_tick then
				--Close all currently open gates
				for x=1,(global.mazesize*2)-1 do
					for y=1,(global.mazesize*2)-1 do
						if global.mazes[1][x][y] ~= "CELL" and global.mazes[1][x][y] ~= "BLANK" then
							if global.mazes[1][x][y] then
								global.control_behaviors[x][y].circuit_condition = {condition = {comparator="≠", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = false}
							end
						end
					end
				end
				--Choose new gates to be open
				build_logical_mazes() --luacheck: globals build_logical_mazes
				--Open the new gates
				for x=1,(global.mazesize*2)-1 do
					for y=1,(global.mazesize*2)-1 do
						if global.mazes[1][x][y] ~= "CELL" and global.mazes[1][x][y] ~= "BLANK" then
							if global.mazes[1][x][y] then
								global.control_behaviors[x][y].circuit_condition = {condition = {comparator="=", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = true}
							end
						end
					end
				end
			elseif global.total_phases > 0 and game.tick~= global.start_tick then
				--Close all currently open gates
				for _,behavior in pairs(global.control_behaviors[global.phase]) do
					behavior.circuit_condition = {condition = {comparator="≠", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = false}
				end
				global.phase = global.phase + 1
				if global.phase == global.total_phases + 1 then global.phase = 1 end
				--Open the gates of the next phase
				for _,behavior in pairs(global.control_behaviors[global.phase]) do
					behavior.circuit_condition = {condition = {comparator="=", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = true}
				end
			end

			--If biters are enabled, take over a single cell of the maze
			if global.biters and game.tick ~= global.start_tick then
				local cell = nil

				while not cell do
					local x = math.random(global.mazesize)
					local y = math.random(global.mazesize)
					if not global.cell_control[x][y] ~= "red" then
						cell = {x=x,y=y}
					end
				end

				if global.cell_control[cell.x][cell.y] == "black" then
					create_biter_base(cell)
					global.cell_control[cell.x][cell.y] = "red"
					game.print({"cells.biter_control", cell.x, cell.y})
					script.raise_event(cell_captured, {previous_team = "black", team="red", cell={x=cell.x,y=cell.y}})
				else
					prepare_attack(cell)
				end
			end

			--If there are no structures in a cell at phase change, change cell to unowned
			for x=1,global.mazesize do
				for y=1,global.mazesize do
					local team = global.cell_control[x][y]
					if team == "Blue" or team == "Green" then
						local top_left = {x = global.top_left.x + ((x - 1) * (global.cellsize + 1)), y = global.top_left.y + ((y - 1) * (global.cellsize + 1))}
						local bottom_right = {x = global.top_left.x + (x * (global.cellsize + 1)), y = global.top_left.y + (y * (global.cellsize + 1))}
						--If there are no force entities other than the Roboport, the Radar, and/or the bonus
						if game.surfaces.Labyrinth.count_entities_filtered{area={top_left, bottom_right}, force=team} - 3 <= 0 then
							--game.print(team .. " has lost control of cell " .. x .. "," .. y .. "!")
							script.raise_event(cell_captured, {previous_team = team, team="black", cell={x=x,y=y}})
							global.cell_control[x][y] = "black"
							global.capturing[x][y] = {team = "none", amount = 0}
						end
					end
				end
			end

			script.raise_event(phase_change, {phase=global.phase})
		end
	
		--Every second, check for cell control and update time to phase change GUI
		if game.tick % 60 == 0 then
			--Update time until phase change GUI
			local delay_in_ticks = global.delay * 60 * 60
			local modulo = (game.tick - global.start_tick) % delay_in_ticks
			local time_to_change = delay_in_ticks - modulo
			if time_to_change == 0 then time_to_change = delay_in_ticks end
			local minutes = math.floor((time_to_change % 60^3) / 60^2)
			local seconds = math.floor((time_to_change % 60^2) / 60)
			if seconds < 10 then seconds = "0" .. seconds end
			local timestamp = minutes .. ":" .. seconds
			for _,p in pairs(game.players) do
				p.gui.top.labyrinth_status.labyrinth_status_table.labyrinth_status_toggle_frame.labyrinth_status_toggle_table.labyrinth_timer.caption = "Time Until Next Phase Change: " .. timestamp
			end

			--Cell control checking
			for x=1,global.mazesize do
				for y=1,global.mazesize do
					if not global.started then return end --The game may have ended between cells, leave the function if so
					local team = global.cell_control[x][y]
					local top_left = {x = global.top_left.x + ((x - 1) * (global.cellsize + 1)) + 0.5, y = global.top_left.y + ((y - 1) * (global.cellsize + 1)) + 0.5}
					local bottom_right = {x = global.top_left.x + (x * (global.cellsize + 1)), y = global.top_left.y + (y * (global.cellsize + 1))}
					local center = {x = (top_left.x + bottom_right.x) / 2, y = (top_left.y + bottom_right.y) / 2}
					local capturing_team = global.capturing[x][y].team
					local capturing_term = global.capturing[x][y].amount
					local greens = get_military_assets("Green", top_left, bottom_right)
					local blues = get_military_assets("Blue", top_left, bottom_right)
					local biters = game.surfaces.Labyrinth.count_entities_filtered{force = "enemy", area = {top_left, bottom_right}}

					if biters == 0 and team == "red" then
						global.cell_control[x][y] = "black"
						team = "black"
						game.print({"cells.biter_killed", x, y})
						script.raise_event(cell_captured, {previous_team = "red", team="black", cell={x=x,y=y}})
					end

					if team ~= "red" then
						if greens == 0 and blues == 0 and team ~= "Green" and team ~= "Blue" and (capturing_team == "Green" or capturing_team == "Blue") then
							global.capturing[x][y] = {team = "none", amount = 0}
						elseif blues == 0 and team == "Green" then
							global.capturing[x][y] = {team = "Green", amount = global.capture_time}
						elseif greens == 0 and team == "Blue" then
							global.capturing[x][y] = {team = "Blue", amount = global.capture_time}
						elseif greens > 0 and blues == 0 and capturing_team ~= "Green" and (x <= global.mazesize - 2 or y > 2) then
							--Green team is capturing a cell, as long as the cell is not in the top right corner
							global.capturing[x][y] = {team = "Green", amount = 1}
							game.surfaces.Labyrinth.create_entity{name = "flying-text", color = colors.green, position = center, text = global.capture_time - 1}
						elseif greens > 0 and blues == 0 and capturing_team == "Green" and capturing_term < global.capture_time - 1 then
							--Green team is capturing a cell
							global.capturing[x][y].amount = global.capturing[x][y].amount + 1
							game.surfaces.Labyrinth.create_entity{name = "flying-text", color = colors.green, position = center, text = global.capture_time - global.capturing[x][y].amount}
						elseif greens > 0 and blues == 0 and capturing_team == "Green" and capturing_term == global.capture_time - 1 then
							--Green team has captured a cell
							global.capturing[x][y].amount = global.capturing[x][y].amount + 1
							global.cell_control[x][y] = "Green"
							game.print({"cells.captured", x, y, "Green"})
							for _,ent in pairs(game.surfaces["Labyrinth"].find_entities_filtered{force="Blue", area={top_left,bottom_right}}) do
							    if ent.name:find("under") and ent.neighbours then
							        ent.neighbours.force = "Green"
							    end
						        ent.force = "Green"
							end
							script.raise_event(cell_captured, {previous_team = team, team="Green", cell={x=x,y=y}})
							game.surfaces.Labyrinth.create_entity{name = "flying-text", color = colors.green, position = center, text = {"cells.captured_text"}}
						elseif blues > 0 and greens == 0 and capturing_team ~= "Blue" and (x > 2 or y <= global.mazesize - 2) then
							--Blue team is capturing a cell, as long as the cell is not in the bottom left corner
							global.capturing[x][y] = {team = "Blue", amount = 1}
							game.surfaces.Labyrinth.create_entity{name = "flying-text", color = colors.blue, position = center, text = global.capture_time - 1}
						elseif blues > 0 and greens == 0 and capturing_team == "Blue" and capturing_term < global.capture_time - 1 then
							--Blue team is capturing a cell
							global.capturing[x][y].amount = global.capturing[x][y].amount + 1
							game.surfaces.Labyrinth.create_entity{name = "flying-text", color = colors.blue, position = center, text = global.capture_time - global.capturing[x][y].amount}
						elseif blues > 0 and greens == 0 and capturing_team == "Blue" and capturing_term == global.capture_time - 1 then
							--Blue team has captured a cell
							global.capturing[x][y] = {team = "none", amount = 0}
							global.cell_control[x][y] = "Blue"
							game.print({"cells.captured", x, y, "Blue"})
							for _,ent in pairs(game.surfaces["Labyrinth"].find_entities_filtered{force="Green", area={top_left,bottom_right}}) do
							    if ent.name:find("under") and ent.neighbours then
							        ent.neighbours.force = "Blue"
							    end
								ent.force = "Blue"
							end
							script.raise_event(cell_captured, {previous_team = team, team="Blue", cell={x=x,y=y}})
							game.surfaces.Labyrinth.create_entity{name = "flying-text", color = colors.blue, position = center, text = {"cells.captured_text"}}
						end
					end
					script.raise_event(cell_processed, {cell={x=x,y=y}})
				end
			end
		end
	end
end)

--luacheck: ignore begin_round
function begin_round()
	for _,p in pairs(game.players) do if p.gui.center.labyrinth_config then p.gui.center.labyrinth_config.destroy() end end
	reset_globals()
	for x=1,global.mazesize do
		global.cell_control[x] = {}
		global.capturing[x] = {}
		global.bonuses[x] = {}
		for y=1,global.mazesize do
			--Set the inital control of each cell to black, which represents neutral/unclaimed
			global.cell_control[x][y] = "black"
			global.capturing[x][y] = {team="none", amount=0}
			global.bonuses[x][y] = "none"
		end
	end
	
	--Find the corners of the Labyrinth
	local length_tiles = (global.mazesize * global.cellsize) + global.mazesize
	global.top_left = {x = -1 * math.floor(length_tiles / 2), y = -1 * math.floor(length_tiles / 2)}
	global.bottom_right = {x = math.ceil(length_tiles / 2), y = math.ceil(length_tiles / 2)}
	
	if not game.surfaces["Labyrinth"] then
		--Create a new Labyrinth surface with the correct settings
		local mapgensettings = game.surfaces[1].map_gen_settings
		mapgensettings.water = "none"
		for resource,_ in pairs(mapgensettings.autoplace_controls) do
			if resource == "enemy-base" and global.biters then
				mapgensettings.autoplace_controls[resource] = {frequency = "very-high", size = "very-big", richness = "very-good"}
			elseif resource == "enemy-base" and not global.biters then
				mapgensettings.autoplace_controls[resource] = {frequency = "none", size = "none", richness = "none"}
			else
				mapgensettings.autoplace_controls[resource] = {frequency = "low", size = "medium", richness = "poor"}
			end
		end
		mapgensettings.seed = math.random(100000000)
		if global.biters then
			mapgensettings.height = length_tiles + (32 - (length_tiles % 32)) + 300
		else
			mapgensettings.height = length_tiles + (32 - (length_tiles % 32))
		end
		mapgensettings.width = 0 --Infinite
		mapgensettings.starting_area = "none"
		
		game.create_surface("Labyrinth", mapgensettings)
	end
	game.surfaces["Labyrinth"].request_to_generate_chunks({0,0}, math.ceil((((global.mazesize * global.cellsize) + global.mazesize) / 2) / 32) + 4)
	global.generating = true
end

Event.register(defines.events.on_built_entity, function(event)
	local entity = event.created_entity
	if not entity or not entity.valid then return end
	local player = game.players[event.player_index]
	if player.surface.name ~= "Labyrinth" then return end
	if entity.type == "car" then
		--Prevent a player from placing a car next to a wall, which prevents leapfrogging over walls
		local top_left = {x = entity.position.x - 2, y = entity.position.y - 2}
		local bottom_right = {x = entity.position.x + 2, y = entity.position.y + 2}
		if entity.surface.count_entities_filtered{name="stone-wall", area={top_left, bottom_right}, force="Labyrinth"} > 0 then
			player.insert({name=entity.name, count=1})
			entity.destroy()
			player.print({"warnings.vehicles1"})
		end
	else
		--Prevent a player from building anything in a cell they don't own
		local cell = find_cell(entity.position)
		if cell and entity.force.name ~= global.cell_control[cell.x][cell.y] then
			if entity.name ~= "entity-ghost" then
				player.insert({name=entity.name, count=1})
			end
			entity.destroy()
			player.print({"warnings.entities"})
		elseif cell and (entity.name == "roboport" or entity.name == "radar") then
			if entity.name ~= "entity-ghost" then
				player.insert({name=entity.name, count=1})
			end
			local name = entity.name
			entity.destroy()
			player.print({"warnings.roboradar", name})
		end
	end
end)

--Prevents players from parking a car next to a wall, which prevents leapfrogging over walls
Event.register(defines.events.on_player_driving_changed_state, function(event)
	local player = game.players[event.player_index]
	if player.surface.name ~= "Labyrinth" then return end
	if not player.character.active then player.character.active = true end
	if player.vehicle then return end --Don't worry about a player entering a car
	local top_left = {x = player.position.x - 3.25, y = player.position.y - 3.75}
	local bottom_right = {x = player.position.x + 3.25, y = player.position.y + 3.75}
	if player.surface.count_entities_filtered{name="stone-wall", force="Labyrinth", area={top_left, bottom_right}} > 0 then
		local top_left_2 = {x = player.position.x - 2.5, y = player.position.y - 2.75}
		local bottom_right_2 = {x = player.position.x + 2.5, y = player.position.y + 2.75}
		local vehicles = player.surface.find_entities_filtered{type="car", force=player.force.name, area={top_left_2, bottom_right_2}}
		--Vechicles will always contain at least one vehicle, as vehicles deposit players no more than 2.5 tiles away
		local vehicle = nil
		if #vehicles == 1 then
			vehicle = vehicles[1]
		else
			--Attempt to figure out which vehicle is the correct one to teleport a player to
			for _,car in pairs(vehicles) do
				local distancex = car.position.x - player.position.x
				local distancey = car.position.y - player.position.y
				local distance = math.floor(math.sqrt(distancex^2 + distancey^2) + 0.02)
				if distance == 2 then
					vehicle = car
					break
				end
			end
		end
		player.print({"warnings.vehicles2"})
		player.teleport(vehicle.position)
		player.character.active = false --Makes it to where a player can only reenter the car
	end
end)

Event.register(defines.events.on_entity_died, function(event)
	if not event.entity or not event.entity.valid then return end
	if event.entity.name == "biter-spawner" or event.entity.name == "spitter-spawner" then
		global.spawners = global.spawners - 1
		if global.spawners == 0 then
			game.print({"warnings.biters"})
			global.biters = false
		end
	end
end)

Event.register(-2, function()
	if not global.started then return end
	print("PVPROUND$ongoing," .. global.round_number .. ",Green,Blue")
end)

Event.register(defines.events.on_console_command, function(event)
	if event.command == "color" then
		local player = game.players[event.player_index]
		if player.force == "Green" then
			player.color = colors.green
		elseif player.force == "Blue" then
			player.color = colors.blue
		end
		player.print({"warnings.color"})
	end
end)
