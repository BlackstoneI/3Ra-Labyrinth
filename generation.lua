--Labyrinth - A Factorio PvP Scenario
--Created by zackman0010 of 3RaGaming
--This file holds the functions that involve generating the Labyrinth

require "locale/utils/event"
require "bonuses"
require "radars"

game_started = game_started or script.generate_event_name()

local function copy_edge(original)
	local copy = {}
	copy[1] = {}
	copy[2] = {}
	copy[1][1] = original[1][1]
	copy[1][2] = original[1][2]
	copy[2] = original[2]
	return copy
end

local function generate_maze()
	local walls = {}
	local cells = {}
	local maze = {}
	local x = math.random(global.mazesize)
	local y = math.random(global.mazesize)
	cells[x] = {}
	cells[x][y] = true
	if x > 1 then
		table.insert(walls, {{x,y}, "x-"})
	end
	if y > 1 then
		table.insert(walls, {{x,y}, "y-"})
	end
	if x < global.mazesize then
		table.insert(walls, {{x,y}, "x+"})
	end
	if y < global.mazesize then
		table.insert(walls, {{x,y}, "y+"})
	end
	while true do
		if #walls == 0 then break end
		local current = math.random(#walls)
		x = walls[current][1][1]
		y = walls[current][1][2]
		local direction = walls[current][2]
		if direction == "x+" then x = x + 1
		elseif direction == "y+" then y = y + 1
		elseif direction == "x-" then x = x - 1
		elseif direction == "y-" then y = y - 1
		end
		if cells[x] and cells[x][y] then
			table.remove(walls, current)
		else
			table.insert(maze, copy_edge(walls[current]))
			cells[x] = cells[x] or {}
			cells[x][y] = true
			if x > 1 then
				table.insert(walls, {{x,y}, "x-"})
			end
			if y > 1 then
				table.insert(walls, {{x,y}, "y-"})
			end
			if x < global.mazesize then
				table.insert(walls, {{x,y}, "x+"})
			end
			if y < global.mazesize then
				table.insert(walls, {{x,y}, "y+"})
			end
			table.remove(walls, current)
		end
	end
	return maze
end

function build_logical_mazes()
	local total_phases = global.total_phases
	if total_phases == 0 then total_phases = 1 end
	global.mazes = {}
	for i=1,total_phases do
		global.mazes[i] = {}
		for x=1,(global.mazesize*2)-1 do
			--Create a blank maze table. In a 3x3 maze, [1][1] is a cell, [1][2] is an edge, [2][1] is an edge, [2][2] is nothing, etc
			--By default, the edges are set to false. Setting an edge to true causes the gate to be open in-game
			global.mazes[i][x] = {}
			for y=1,(global.mazesize*2)-1 do
				if x % 2 == 0 then
					if y % 2 == 1 then
						global.mazes[i][x][y] = false
					else
						global.mazes[i][x][y] = "BLANK"
					end
				elseif x % 2 == 1 then
					if y % 2 == 0 then
						global.mazes[i][x][y] = false
					else
						global.mazes[i][x][y] = "CELL"
					end
				end
			end
		end
		--Retrieve a list of edges that are a part of the maze
		local edge_list = generate_maze()
		for _,edge in pairs(edge_list) do
			--For each edge in the edge list created, set the corresponding cell in the maze table to true.
			local x = (edge[1][1] * 2) - 1
			local y = (edge[1][2] * 2) - 1
			local direction = edge[2]
			if direction == "x+" then x = x + 1
			elseif direction == "y+" then y = y + 1
			elseif direction == "x-" then x = x - 1
			elseif direction == "y-" then y = y - 1
			end
			global.mazes[i][x][y] = true
		end
	end
end

local function build_physical_maze()
	local length_tiles = (global.mazesize * global.cellsize) + global.mazesize

	--Set all Labyrinth tiles to concrete
	local tiles = {}
	for x = global.top_left.x - 5, global.bottom_right.x + 5 do
		for y = global.top_left.y, global.bottom_right.y do
			table.insert(tiles, {name = "concrete", position = {x,y}})
		end
	end
	game.surfaces["Labyrinth"].set_tiles(tiles)
	
	--Destroy all existing entities in the Labyrinth, plus biters a chunk above and below (to prevent worm range from reaching into the Labyrinth)
	for _,ent in pairs(game.surfaces["Labyrinth"].find_entities_filtered{area={{x = global.top_left.x - 8, y = global.top_left.y - 32}, {x = global.bottom_right.x + 8, y = global.bottom_right.y + 32}}}) do
		ent.destroy()
	end
	
	--Create the cells
	local next_vertical = global.top_left.x
	local next_horizontal = global.top_left.y
	local gate_walls = {}
	for x = global.top_left.x, global.bottom_right.x do
		for y = global.top_left.y, global.bottom_right.y do
			if y == next_horizontal then
				if (
					(y > global.top_left.y and y < global.bottom_right.y)
					or (y == global.top_left.y and next_vertical == (global.top_left.x + global.cellsize + 1) and global.biters)
					or (y == global.bottom_right.y and next_vertical == global.bottom_right.x and global.biters)
				) and (next_vertical - x) == math.ceil(global.cellsize / 2) then
					--Create a horizontal gate. Two wide if global.cellsize is even, three wide if global.cellsize is odd
					local gate = game.surfaces["Labyrinth"].create_entity({name="gate", force="Labyrinth", position={x,y}, direction=defines.direction.west})
					gate.destructible = false
					local gatex = (2 * ((next_vertical + math.floor(length_tiles / 2)) / (global.cellsize + 1))) - 1
					local gatey = (2 * ((next_horizontal + math.floor(length_tiles / 2)) / (global.cellsize + 1)))
					local wall_position = gate.position
					wall_position.x = wall_position.x - 1
					game.surfaces["Labyrinth"].find_entity("stone-wall", wall_position).destroy()
					game.surfaces["Labyrinth"].create_entity({name="gate", force="Labyrinth", position=wall_position, direction=defines.direction.west}).destructible = false
					wall_position.x = wall_position.x - 1
					local wall = game.surfaces["Labyrinth"].find_entity("stone-wall", wall_position)
					table.insert(gate_walls, {wall, "x"})
					local behavior = wall.get_or_create_control_behavior()
					behavior.open_gate = true
					if y > global.top_left.y and y < global.bottom_right.y then
						--Gate is internal, add the behavior to the list for maze operation
						if global.total_phases > 0 then
							for i,maze in ipairs(global.mazes) do
								global.control_behaviors[i] = global.control_behaviors[i] or {}
								if maze[gatex][gatey] then
									table.insert(global.control_behaviors[i], behavior)
								end
							end
						else
							global.control_behaviors[gatex] = global.control_behaviors[gatex] or {}
							global.control_behaviors[gatex][gatey] = behavior
						end
					else
						--Gate is entry, permanently unlock the gate
						behavior.circuit_condition = {condition = {comparator="=", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = true}
					end
					if global.cellsize % 2 == 1 then
						wall_position.x = wall_position.x + 3
						game.surfaces["Labyrinth"].create_entity({name="gate", force="Labyrinth", position=wall_position, direction=defines.direction.west}).destructible = false
					end
				else
					local wall = game.surfaces["Labyrinth"].create_entity({name="stone-wall", force="Labyrinth", position={x,y}})
					if wall then wall.destructible = false end
				end
				next_horizontal = next_horizontal + global.cellsize + 1
			elseif x == next_vertical then
				if (
					(x > global.top_left.x and x < global.bottom_right.x)
					or (x == global.top_left.x and next_horizontal == global.bottom_right.y)
					or (x == global.bottom_right.x and next_horizontal == (global.top_left.y + global.cellsize + 1))
				) and (next_horizontal - y) == math.ceil(global.cellsize / 2) then
					--Create a vertical gate. Two wide if global.cellsize is even, three wide if global.cellsize is odd
					--Also creates entry gates in lower-left cell and upper-right cell. Same sizes as the internal gates.
					local gate = game.surfaces["Labyrinth"].create_entity({name="gate", force="Labyrinth", position={x,y}, direction=defines.direction.north})
					gate.destructible = false
					local gatex = (2 * ((next_vertical + math.floor(length_tiles / 2)) / (global.cellsize + 1)))
					local gatey = (2 * ((next_horizontal + math.floor(length_tiles / 2)) / (global.cellsize + 1))) - 1
					local wall_position = gate.position
					wall_position.y = wall_position.y - 1
					game.surfaces["Labyrinth"].find_entity("stone-wall", wall_position).destroy()
					game.surfaces["Labyrinth"].create_entity({name="gate", force="Labyrinth", position=wall_position, direction=defines.direction.north}).destructible = false
					wall_position.y = wall_position.y - 1
					local wall = game.surfaces["Labyrinth"].find_entity("stone-wall", wall_position)
					table.insert(gate_walls, {wall, "y"})
					local behavior = wall.get_or_create_control_behavior()
					behavior.open_gate = true
					if x > global.top_left.x and x < global.bottom_right.x then
						--Gate is internal, add the behavior to the list for maze operation
						if global.total_phases > 0 then
							for i,maze in ipairs(global.mazes) do
								global.control_behaviors[i] = global.control_behaviors[i] or {}
								if maze[gatex][gatey] then
									table.insert(global.control_behaviors[i], behavior)
								end
							end
						else
							global.control_behaviors[gatex] = global.control_behaviors[gatex] or {}
							global.control_behaviors[gatex][gatey] = behavior
						end
					else
						--Gate is entry, permanently unlock the gate
						behavior.circuit_condition = {condition = {comparator="=", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = true}
					end
					if global.cellsize % 2 == 1 then
						wall_position.y = wall_position.y + 3
						game.surfaces["Labyrinth"].create_entity({name="gate", force="Labyrinth", position=wall_position, direction=defines.direction.north}).destructible = false
					end
				else
					local wall = game.surfaces["Labyrinth"].create_entity({name="stone-wall", force="Labyrinth", position={x,y}})
					if wall then wall.destructible = false end
				end
			end
		end
		if x == next_vertical then next_vertical = next_vertical + global.cellsize + 1 end
		next_horizontal = global.top_left.y
	end
	
	--Set up circuit networks
	for _,gate in pairs(gate_walls) do
		local wall1 = gate[1]
		local direction = gate[2]
		local other_position = wall1.position
		if direction == "x" and global.cellsize % 2 == 0 then other_position.x = other_position.x + 3
		elseif direction == "y" and global.cellsize % 2 == 0 then other_position.y = other_position.y + 3
		elseif direction == "x" and global.cellsize % 2 == 1 then other_position.x = other_position.x + 4
		elseif direction == "y" and global.cellsize % 2 == 1 then other_position.y = other_position.y + 4
		end
		local wall2 = game.surfaces["Labyrinth"].find_entity("stone-wall", other_position)
		wall1.connect_neighbour({wire=defines.wire_type.green, target_entity=wall2})
	end
end

--The complete process of creating the labyrinth
local function create_labyrinth()
	build_logical_mazes()
	build_physical_maze()

	--Add the bonuses to the Labyrinth
	--luacheck: globals add_roboports add_radars add_bonuses
	add_roboports()
	add_radars()
	add_bonuses()

	--Set force-related things
	local center_y = (global.bottom_right.y + global.top_left.y) / 2
	game.forces["Green"].chart_all("Labyrinth")
	game.forces["Blue"].chart_all("Labyrinth")
	game.forces["Green"].set_spawn_position({x = global.top_left.x - 96, y = center_y}, "Labyrinth")
	game.forces["Blue"].set_spawn_position({x = global.bottom_right.x + 96, y = center_y}, "Labyrinth")

	--Open the gates of the first phase
	if global.total_phases == 0 then
		for x=1,(global.mazesize*2)-1 do
			for y=1,(global.mazesize*2)-1 do
				if global.mazes[1][x][y] ~= "CELL" and global.mazes[1][x][y] ~= "BLANK" then
					if global.mazes[1][x][y] then
						global.control_behaviors[x][y].circuit_condition = {condition = {comparator="=", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = true}
					end
				end
			end
		end
	else
		for _,behavior in pairs(global.control_behaviors[1]) do
			behavior.circuit_condition = {condition = {comparator="=", first_signal={type="virtual",name="signal-O"}, second_signal={type="virtual",name="signal-O"}}, fulfilled = true}
		end
	end
	global.start_tick = game.tick + 1
	global.started = true
	global.phase = 1

	global.spawners = global.spawners + game.surfaces.Labyrinth.count_entities_filtered{name="biter-spawner", force="enemy"}
	global.spawners = global.spawners + game.surfaces.Labyrinth.count_entities_filtered{name="spitter-spawner", force="enemy"}

	global.round_number = global.round_number + 1
	print("PVPROUND$begin," .. global.round_number .. ",Green,Blue")

	script.raise_event(game_started, {})
end

Event.register(defines.events.on_tick, function(event) --luacheck: ignore event
	if global.generating then
		--Find the corners of the Labyrinth on the chunk grid
		local top_left_chunk = {x = math.floor(global.top_left.x / 32), y = math.floor(global.top_left.y / 32)}
		local bottom_right_chunk = {x = math.floor(global.bottom_right.x / 32), y = math.floor(global.bottom_right.y / 32)}
		local complete = false
		
		--Check to see if all the Labyrinth chunks have been generated already
		for x=top_left_chunk.x,bottom_right_chunk.x do
			for y=top_left_chunk.y,bottom_right_chunk.y do
				complete = false
				if not game.surfaces["Labyrinth"].is_chunk_generated({x=x,y=y}) then break end
				complete = true
			end
		end
		
		--Set each chunk of the Labyrinth to say that entities have already been generated.
		--Normally, the map generator only generates and corrects tiles. Entities are generated when a player is nearby
		--This step tells the map generator that entities have already been placed, preventing it from placing ore and trees inside the Labyrinth
		if complete then
			global.generating = false
			for x=top_left_chunk.x,bottom_right_chunk.x do
				for y=top_left_chunk.y,bottom_right_chunk.y do
					game.surfaces["Labyrinth"].set_chunk_generated_status({x=x,y=y}, defines.chunk_generated_status.entities)
				end
			end
			create_labyrinth()
		end
	end
end)

Event.register(defines.events.on_chunk_generated, function(event)
	if (not global.generating and not global.started) or event.surface.name ~= "Labyrinth" then return end
	local tiles = {}
	for x = event.area.left_top.x, event.area.right_bottom.x do
		for y = event.area.left_top.y, event.area.right_bottom.y do
			if global.biters then
				if ((x < global.top_left.x) and (y < global.top_left.y))
				or ((x < global.top_left.x) and (y > global.bottom_right.y))
				or ((x > global.bottom_right.x) and (y < global.top_left.y))
				or ((x > global.bottom_right.x) and (y > global.bottom_right.y)) then
					table.insert(tiles, {name = "out-of-map", position = {x,y}})
				end
			else
				if y < global.top_left.y or y > global.bottom_right.y then
					table.insert(tiles, {name = "out-of-map", position = {x,y}})
				end
			end
		end
	end
	if event.area.left_top.x < global.top_left.x or event.area.right_bottom.x > global.bottom_right.x then
		for _,enemy in pairs(game.surfaces["Labyrinth"].find_entities_filtered{area=event.area, force="enemy"}) do
			enemy.destroy()
		end
	end
	event.surface.set_tiles(tiles)
end)
